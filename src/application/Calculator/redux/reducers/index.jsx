import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import CalculatorReducer from './CalculatorReducer';


const reducers = {
    // ... your other reducers here ...
    'calculator': CalculatorReducer,
    form: formReducer
};

const reducer = combineReducers(reducers);

export default reducer;
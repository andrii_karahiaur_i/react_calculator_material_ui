import {
    CALCULATOR_DATA,
    CALCULATOR_TOTAL
} from '../actions';

const CalculatorReducer = (state = {}, action) => {
    switch(action.type){
        case CALCULATOR_DATA:
            return {...state, ...action.data};
        case CALCULATOR_TOTAL:
            return {...state, ...action.data};
        default:
            return state;
    }
};

export default CalculatorReducer;
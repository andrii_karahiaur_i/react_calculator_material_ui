const CALCULATOR_DATA = 'CALCULATOR_DATA';
const CALCULATOR_TOTAL = 'CALCULATOR_TOTAL';

const getCalculatorData = (data) => {
    return{
        type: CALCULATOR_DATA,
        data: data
    }
};

const getCalculatorTotal = (data) => {
    return{
        type: CALCULATOR_TOTAL,
        data: data
    }
};


export {
    CALCULATOR_DATA,
    CALCULATOR_TOTAL,
    getCalculatorData,
    getCalculatorTotal
};
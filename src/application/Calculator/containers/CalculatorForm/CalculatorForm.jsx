import React, {Fragment, useEffect, useState} from "react";
import {Field} from "redux-form";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Radio from '@material-ui/core/Radio';

import SelectField from "../../components/SelectField";
import {required} from "../../validate/validate";
import InputTextField from "../../components/InputTextField";
import CheckboxField from "../../components/CheckboxField";
import RadioFieldWays from "../../components/RadioFieldWays";
import Button from "../../components/Buttons";
import {createStyles, makeStyles, Theme} from "@material-ui/core";
import axios from "axios";

// const maxlength48 = maxLengthCreator(48);
// const maxlength12 = maxLengthCreator(12);
// const minlength1 = minLengthCreator(1);

const url_directory =  'http://localhost:3000/database/api.json';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {},
        flexDirection: {
            flexDirection: "row"
        },
        marginGrid: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
        marginBox: {
            marginLeft: theme.spacing(-1),
            marginRight: theme.spacing(-1),
        },
    })
);

const CalculatorForm = (props) => {
    const classes = useStyles();
    const { handleSubmit, pristine, submitting } = props;
    const [directory, setDirectory] = useState({});

    const countries_from = directory.country ? directory.country.filter(country => country.id === 44 || country.id === 240) : [];
    const countries_to = directory.country ? directory.country.filter(country => country.id === 232) : [];
    const currencies = directory.currency ? directory.currency.map((val) => ({id: val.id, name: val.code})) : [];

    const simple = props.dimensions.simple ? props.dimensions.simple : false;
    const dimensions = simple.values ? simple.values.dimensions : false;
    const custom = simple.values ? simple.values.custom : false;

    // Axios
    useEffect(() => {
        const fetchData = async () => {

            const result = await axios(
                url_directory,
            );
            setDirectory({...result.data});
        };
        fetchData();
    }, []);

    return(
        <Fragment>
            <form onSubmit={handleSubmit}>
                <Box display="flex" alignItems="flex-start" justifyContent="center" flexDirection="row">
                    <Field name={"ways"} component={RadioFieldWays} display="flex" className={classes.flexDirection} >
                        <Radio value="air" label="_Air" />
                        <Radio value="water" label="_Water" />
                    </Field>

                </Box>
                <hr />
                <Container maxWidth="lg">
                    <Box display="flex" alignItems="flex-start" justifyContent="space-between" className={classes.marginBox}>
                        <Grid item xs={3} className={classes.marginGrid}>
                            <Field name={"countries_from"}
                                   component={SelectField}
                                   label="_Countries_from"
                                   validate={[required]}
                                   options={countries_from}
                            />
                        </Grid>
                        <Grid item xs={3} className={classes.marginGrid}>
                            <Field name={"countries_to"}
                                   component={SelectField}
                                   label="_Countries_to"
                                   validate={[required]}
                                   options={countries_to}
                            />
                        </Grid>
                        <Grid item xs={3} className={classes.marginGrid}>
                            <Field name="weight" component={InputTextField} type="number" validate={[required]} label="_Weight" />
                        </Grid>
                        <Grid item xs={3} className={classes.marginGrid}>
                            <Field name={"unit"} component={CheckboxField} label={"_kg/_pounds"} />
                        </Grid>
                    </Box>
                    <hr />
	                <Field name="dimensions" component={CheckboxField} label={"_With_indication_of_dimensions"} />

                    {
                        dimensions
                            ?
                            <Box display="flex" alignItems="flex-start" justifyContent="space-between" className={classes.marginBox}>
                                <Grid item xs={3} className={classes.marginGrid}>
                                    <Field name="length" component={InputTextField} type="number" validate={[required]} label="_Length" />
                                </Grid>
                                <Grid item xs={3} className={classes.marginGrid}>
                                    <Field name="height" component={InputTextField} type="number" validate={[required]} label="_Height" />
                                </Grid>
                                <Grid item xs={3} className={classes.marginGrid}>
                                    <Field name="width" component={InputTextField} type="number" validate={[required]} label="_Width" />
                                </Grid>
                                <Grid item xs={3} className={classes.marginGrid}>
                                    <Field name="unit" component={CheckboxField} label={"_cm/_inches"} />
                                </Grid>
                            </Box>
                            :
                            null

                    }
	                <hr />
                    <Field name="custom" component={CheckboxField} label={"_With_to_customs_duties"} />

                    {
                        custom
                            ?
                            <Box display="flex" alignItems="flex-start" justifyContent="flex-start" className={classes.marginBox}>
                                <Grid item xs={3} className={classes.marginGrid}>
                                    <Field name="assessed_value" component={InputTextField} type="number" validate={[required]} label="_Assessed_value" />
                                </Grid>
                                <Grid item xs={3} className={classes.marginGrid}>
                                    <Field name={"currencies"}
                                           component={SelectField}
                                           label="_Currency"
                                           validate={[required]}
                                           options={currencies}
                                    />
                                </Grid>

                            </Box>
                            :
                            null
                    }
                    <hr/>

                    <Button disabled={pristine || submitting}>_Submit</Button>
                </Container>
            </form>
        </Fragment>
    );
};

export default CalculatorForm
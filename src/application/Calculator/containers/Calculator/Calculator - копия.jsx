import React, {Component, Fragment, useState} from "react";
import Form from "react-jsonschema-form";
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

// import * as _ from 'lodash';

// schemas
// import calculator_form from '../../schemas/calculator_form';
import schema from '../../schemas/calculator_form';
import uiSchema from '../../uiSchema/uiSchema';
import Buttons from "../../components/Buttons";
import './Calculator.scss';
// import {object} from "prop-types";

/*
function calculator__get_ref_value(ref_field_name) { 
    return ref_fhd_calculator[ref_field_name];
}

// https://stackoverflow.com/questions/29568721/getting-dom-node-from-react-child-element

???



function calculator__get_ref_value(refCalculatorElementName) { 
  React.Children.map(this.props.children, (refCalculatorElementName, idx) => {
        return React.cloneElement(refCalculatorElementName, { ref: idx });
  }
}

function calculator__get_calculator_value() { 
    const args = [...arguments]
    return lodash.get(calculator, args); 
   }
    <CALCULTOR ref={refCalculatorElement = React.createRef()}>
        <Tab data-name="water" />
        <Tab data-name="air" />
        onTabClick(tabElement) => <input type="hidden" name="way" value="{tabElement.name}" />
    </CALCULTOR>


    calculator__way([ref_fhd_calculator.way, ref_fhd_calculator.direction]) 
     ==> 
        calculator__get_calculator_value(calculator__get_ref_value('way'), calculator__get_ref_value('direction')) 

        function total_result(calculator) {
            var calcultor_result_array = calculator['formula', 'total'].map((el) => { try { var result = eval(el[Object.keys(el)[0]]) } catch(e) {}  if (typeof result !== 'undefined') { return [Object.keys(el)[0], result] } })
            return calcultor_result_array;
        }
        for (total_result, ... ) {
         +++
        }
*/

/*const calculator_test = {
	"definitions": {
		"formula": {
			weight: "Math.max(ref_fhd_calculator.weigth, calculator__formula__weight_from_size)",
			weight_from_size: "( calculator__get_ref_value('height') * calculator__get_ref_value('width') * calculator__get_ref_value('length') ) / 5000",

			transport_fares: "calculator__formula__weight * ", // !!! calculator__way([ref_fhd_calculator.way, ref_fhd_calculator.direction])",
			tax: "150 >= calculator__formula__transport_fares ?  calculator__formula__transport_fares : (calculator__formula__transport_fares - 150 * 0.32)",
			// total: ["calculator__formula__weight * calculator__formula__transport_fares + calculator__formula__tax"]
			total: [{"__total_weight": "calculator__formula__weight"}, {"__in_total": "calculator__formula__weight * calculator__formula__transport_fares + calculator__formula__tax"}]
// var calcultor_result_array = formula.total.map((el) => { try { var result = eval(el[Object.keys(el)[0]]) } catch(e) {}  if (typeof result !== 'undefined') { return [Object.keys(el)[0], result] } })        
		},
		"way": {
			"water": {
				description: {
					title: "_water",
					icon: "<svg>svg</svg>",
				},
				usa_ua: 0.71,
				ua_usa: 0.82,
			},
			"air": {
				description: {
					title: "_air",
					icon: "<svg>svg</svg>",
				},
				usa_ua: 0.8,
				ua_usa: 0.9,
			}
		},
		"country": [
			{"id": "UKR", "name": "_Ukraine"},
			{"id": "CHN", "name": "_China"},
			{"id": "USA", "name": "_America"}
		],
        "currencies": [
            {"id": "UAH", "name": "_UAH"},
            {"id": "USD", "name": "_USD"},
            {"id": "EUR", "name": "_EUR"},
            {"id": "CNY", "name": "_CNY"}
        ],

	}
};
*/


//==============================================================================================


// function calculator__get_ref_value(refCalculatorElementName) {
// 	React.Children.map(this.props.children, (refCalculatorElementName, idx) => {
// 		return React.cloneElement(refCalculatorElementName, { ref: idx });
// 	}
// }

function calculator__get_ref_value(prop) {
	// const {formElement} = prop.current.Form;
	console.log('calculator__get_ref_value - ', prop.current)
}

function transformErrors(errors) {
    return errors.map(error => {

        switch(error.name) {
            case 'required':
                error.message = "_Is_a_required_property";
                break;

            case 'type':
                error.message  = "_Should_be_number";
                break;

            default:
                return error;
        }

        return error;
    });
}

const Calculator = () => {
	const [tariff, setTariff] = useState(0);
	const [delivery, setDelivery] = useState((0).toFixed(2));
	const [count, setCount] = useState(0);

	function getCount(){
		console.log('OLD Count -- ', count);
		return setCount(count + 1);
	}


	function actionCount(){
		console.log('NEW Count -- ', count);
	}




	function getTariff(schema,formData) {
		const {ways,countries_from,countries_to} = schema;
		// 1
		let way_from_to = [];
		let tariffs = {};

		// selected way
		countries_from.countries.map((country) => formData.total_cost.country_from === country.name ? way_from_to.push(country.id) : null );
		countries_to.countries.map((country) => formData.total_cost.country_to === country.name ? way_from_to.push(country.id) : null );

		// get tariff from way
		try {
			Object.keys(ways).map(name =>
				formData.way === name ? tariffs = {...ways[name].tariff} : null
			);
		}
		catch (error) {console.error(error)}

		// get price from tariff
		try{
			Object.keys(tariffs).forEach(function(rate) {
				if(way_from_to.join("_") === rate) {
					console.log( '1111111111111111 rate - ', rate );
					setTariff( tariffs[rate] ? (tariffs[rate]).toFixed(2) : (0).toFixed(2) );
					return tariff;
				}else{
					return false;
				}
			})
		}
		catch(error){console.error(error);}

		return tariff;
	}

	function getDelivery(formData) {
		console.log( '222222222222222222 tariff - ', tariff );
		try {
			setDelivery( tariff ? (formData.total_cost.weight * tariff).toFixed(2) : (0).toFixed(2) );
		}
		catch(error){console.error(error);}
	}

	function getTotal(schema,formData){
		// console.log( 'getTotal - data - ', data );
		// console.log( 'getTotal - formData - ', formData );
/*		const {ways,countries_from,countries_to} = schema;

		// 1
		let way_from_to = [];
		let tariffs = {};

		// selected way
		countries_from.countries.map((country) => formData.total_cost.country_from === country.name ? way_from_to.push(country.id) : null );
		countries_to.countries.map((country) => formData.total_cost.country_to === country.name ? way_from_to.push(country.id) : null );

		// get tariff from way
		try {
			Object.keys(ways).forEach(function(name) {
				if(formData.way === name){
					tariffs = {...ways[name].tariff};
				}
			});
		}
		catch (error) {console.error(error);}

		// get price from tariff
		try{
			Object.keys(tariffs).forEach(function(rate) {
				let way = way_from_to.join("_");

				return way === rate
					?
					setTariff(() => ((tariffs[rate]).toFixed(2)) )
					:
					null;
			})
		}
		catch(error){console.error(error);}
		console.log( 'tariff - ', tariff );

		setDelivery(() => ( (formData.total_cost.weight * tariff).toFixed(2)) );
		return console.log( 'delivery - ', delivery );*/





		const promise = new Promise( (resolve, reject) => {
			const {ways,countries_from,countries_to} = schema;
			// 1
			let way_from_to = [];
			let tariffs = {};

			// selected way
			countries_from.countries.map((country) => formData.total_cost.country_from === country.name ? way_from_to.push(country.id) : null );
			countries_to.countries.map((country) => formData.total_cost.country_to === country.name ? way_from_to.push(country.id) : null );

			// get tariff from way
			try {
				Object.keys(ways).forEach(function(name) {
					if(formData.way === name){
						tariffs = {...ways[name].tariff};
					}
				});
			}
			catch (error) {console.error(error);}

			if (way_from_to.length > 0) {
				resolve( {"way_from_to": way_from_to, "tariffs": tariffs, } );
			}
			else {
				reject(Error("Promise rejected"));
			}
		});

		promise.then( result => {
			// get price from tariff
			console.log( 'tariffs --', result.tariffs );
			console.log( 'way_from_to --', result.way_from_to );

			try{
				Object.keys(result.tariffs).forEach(function(rate) {

					if(result.way_from_to.join("_") === rate) {
						console.log( '1111111111111111 rate - ', result.tariffs[rate] );
						setTariff( result.tariffs[rate] ? (result.tariffs[rate]).toFixed(2) : (0).toFixed(2) );
						return tariff;
					}
				})
			}
			catch(error){console.error(error);}

		}).then( () => {
			console.log( '222222222222222222 tariff - ', tariff );
			try {
				setDelivery( tariff ? (formData.total_cost.weight * tariff).toFixed(2) : (0).toFixed(2) );
			}
			catch(error){console.error(error);}

		}, function(error) {
			console.error(error)
		});
	}

	function onChange(event){
    	console.log('onChange - ', event.formData);
		// calculator__get_ref_value(this.refCalculator)
	}

	function onSubmit(event){
		// getTotal(schema.definitions, event.formData);
		getCount();

		const promise = new Promise( (resolve, reject) => {
			schema ? resolve(schema) : reject(Error("Promise rejected"));
		});
		promise.then( (resolve) => {
			getCount();
			getTariff(resolve.definitions, event.formData)
		}).then(() => {
			actionCount();
				console.log( '3333333333333 tariff - ', tariff );
			getDelivery(event.formData)
		},
			function(error) {
			console.error(error)
		});

        console.log( 'Submit formData- ',  event.formData);
    }

	return(
		<Fragment>

			{
				<Form
					// ref={this.refCalculator}
					schema={schema}
					uiSchema={uiSchema}
					className="form-default"
					// formData={formData)}
					// validate={validate}
					noHtml5Validate
					onChange={onChange}
					onSubmit={onSubmit}
					// onError={log("errors")}
					liveValidate
					transformErrors={transformErrors}
					showErrorList={false}
				>
					<Grid
						container
						direction="row"
						justify="center"
						alignItems="center"
					>
						<Buttons />
					</Grid>
				</Form>
			}

			<Container fixed>
				{(count).toFixed(2)}
				<div>
					{tariff}
				</div>
				<Grid
					container
					direction="row"
					justify="space-between"
					alignItems="center"
				>
					<p>Доставка (воздухом) </p>
					<span>$ {delivery}</span>
				</Grid>
				<Grid
					container
					direction="row"
					justify="space-between"
					alignItems="center"
				>
					<p>Таможенное оформление </p>
					<span>$ 0.00</span>
				</Grid>
				<Grid
					container
					direction="row"
					justify="space-between"
					alignItems="center"
				>
					<p>Страховка </p>
					<span>$ 0.00</span>
				</Grid>
				<Grid
					container
					direction="row"
					justify="space-between"
					alignItems="center"
				>
					<p>Итого:</p>
					<b>$ 0.00</b>
				</Grid>

			</Container>
		</Fragment>
	);
};

export default Calculator;
import React, {Fragment, useState, useEffect} from "react";
import {connect} from "react-redux";
import {reduxForm} from 'redux-form';
import axios from 'axios';

import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import 'typeface-roboto'; // 300, 400, 500, 700 - for Default

import {getCalculatorData, getCalculatorTotal} from '../../redux/actions/index';
import CalculatorForm from '../CalculatorForm/index';
// import api from '../../api/rest_api';
const url_directory =  'http://localhost:3000/database/api.json';
const url_calculatorData =  'http://localhost:3000/database/calculatorData.json';

const CalculatorReduxForm = reduxForm({form: 'simple'})(CalculatorForm);

const Calculator = (props) => {
	const [tariff, setTariff] = useState(0);
	const [customDutyUSD, setCustomDutyUSD] = useState(1);
	const [total, setTotal] = useState(0);
	const [taxation, setTaxation] = useState(0);
	const [belay, setBelay] = useState(0);
	const [currency, setCurrency] = useState([]);
	const [calculatorData, setCalculatorData] = useState({});

	// Axios
	useEffect(() => {
		const fetchData = async () => {

			const  result_directory = await axios(
				url_directory ,
			);
			const result_calculatorData = await axios(
				url_calculatorData,
			);
			setCurrency( [].concat(result_directory.data.currency) );
			setCalculatorData( result_calculatorData.data );
		};
		fetchData();
	}, []);

	const onChange = (formData) => {
		console.log('onChange == ', formData )
	};

	const onSubmit = (formData) => {
		const {inch,
			volumetric_weight,
			customer_pounds,
			amount_customs_duty,
			part_amount_customs_duty,
			factor_duty,
			VAT,
			insurance
		} = calculatorData;

		console.log( 'onSubmit == ', formData );
		props.sendCalculatorData(formData);

		try {

			// 1) Total
			const {ways} = calculatorData;
			Object.keys(formData).map(key => {
				if( key === "ways" ){
					// get tariff for formula
					let tariff = ways[formData[key]].tariff;
					tariff.map(rate => {
						if( formData.countries_from === rate.countries_from && formData.countries_to === rate.countries_to){
							if( formData.unit  ){
								setTariff( (rate.price).toFixed(2) );
								return setTotal( (formData.weight*rate.price).toFixed(2) )
							}
							else{
								let pounds = (formData.weight*customer_pounds).toFixed(2);
								setTariff( (rate.price).toFixed(2) );
								return setTotal( (pounds*rate.price).toFixed(2) )
							}
						}
						else{
							return false;
						}
					})
				}
				return false;
			});

			// 2) Dimensions ~ длина (см) × ширина (см) × высота (см) / 5000 = объёмный вес (кг)
				if( formData.dimensions ) {
					const volumetric_weight_result = (formData.length * formData.height * formData.width)/volumetric_weight;
					const volumetric_weight_result_inch = ((formData.length*inch) * (formData.height*inch) * (formData.width*inch))/volumetric_weight;
					if ( formData.unit ) {
						return setTotal( (tariff*volumetric_weight_result_inch).toFixed(2) )
					}else{
						return setTotal( (tariff*volumetric_weight_result).toFixed(2) )
					}
				}else{
					// destroy same state form-redux
					delete formData['length'];
					delete formData['height'];
					delete formData['width'];
				}

				// 3) Customs Duties
				if( formData.custom ){
					let customDutyEUR = 0;

					try{
						currency.map(val => {
							if(formData.currencies === val.id){
								setCustomDutyUSD(val.usd);
								return customDutyEUR = (formData.assessed_value * val.eur).toFixed(2);
							}
							else{
								return customDutyEUR;
							}
						})
					}
					catch(e){
						console.error(e)
					}

					// actuarial
					setBelay((customDutyEUR * insurance).toFixed(2) );
					// 3.1) Ф.С. > 150€ { С.Н. = 50€*0.2 + (Ф.С. - 150)*0,32}
					if(customDutyEUR > amount_customs_duty){
						let result = (part_amount_customs_duty * VAT) + (customDutyEUR - amount_customs_duty) * factor_duty;
						return setTaxation( (result*customDutyUSD).toFixed(2) )
					}
					// 3.2) Ф.С. больше 100€ и меньше 150€{ С.Н. = (Ф.С.-100)*0.2}
					else if( customDutyEUR <= amount_customs_duty && customDutyEUR > 100 ){
						let result = (customDutyEUR - 100) * VAT;
						return setTaxation( (result*customDutyUSD).toFixed(2) )
					}
					// 3) Ф.С. =< 100€ { С.Н. = 0 }
					else{
						return setTaxation( (0).toFixed(2) )
					}
				}else{
					setBelay((0).toFixed(2));
					setTaxation((0).toFixed(2));

					// destroy same state form-redux
					delete formData['assessed_value'];
					delete formData['currencies'];
				}
		}
		catch(e){
			return console.error(e)
		}
	};
	return(
		<Fragment>
			<Container maxWidth="lg">
				<h1>Calculator</h1>
			</Container>
			<CalculatorReduxForm onSubmit={onSubmit} onChange={onChange} dimensions={props.calculator.form} />

			<Container maxWidth="lg">
				<Box display="flex" alignItems="flex-start" justifyContent="space-between">
					<Grid item xs={6}>
						<Typography variant="body1" gutterBottom>
							Доставка (воздухом)
						</Typography>
					</Grid>
					<Grid item xs={6}>
						<Typography variant="body1" gutterBottom>
							{total || 0}$
						</Typography>
					</Grid>
				</Box>
				<Box display="flex" alignItems="flex-start" justifyContent="space-between">
					<Grid item xs={6}>
						<Typography variant="body1" gutterBottom>
							Таможенные платежи
						</Typography>
					</Grid>
					<Grid item xs={6}>
						<Typography variant="body1" gutterBottom>
							{taxation || 0}$
						</Typography>
					</Grid>
				</Box>
				<Box display="flex" alignItems="flex-start" justifyContent="space-between">
					<Grid item xs={6}>
						<Typography variant="body1" gutterBottom>
							Страховка (зависит от оценочной стоимости)
						</Typography>
					</Grid>
					<Grid item xs={6}>
						<Typography variant="body1" gutterBottom>
							{belay || 0}$
						</Typography>
					</Grid>
				</Box>
			</Container>
		</Fragment>
	)
};

const mapDispatchToProps = (dispatch) => {
	return {
		'sendCalculatorData': (formData) => {
			dispatch(getCalculatorData(formData))
		},
		'sendCalculatorTotal': (data) => {
			dispatch(getCalculatorTotal(data))
		}
	}
};

const mapStateToProps = (state) => {
	return{
		'calculator': state
	}
};

export default connect(mapStateToProps,mapDispatchToProps)(Calculator);

import React from 'react';
import { Provider } from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import CssBaseline from '@material-ui/core/CssBaseline';
import PropTypes from 'prop-types';

import reducer from './redux/reducers';
import Layout from './hoc/Layout';
import Calculator from './containers/Calculator';

const store = createStore(
	reducer,
	composeWithDevTools(applyMiddleware(thunk))
);

const App = () => {
    return(
        <React.Fragment>
            <CssBaseline />
            <Provider store={store}>
                <Layout>
                    <Calculator />
                </Layout>
            </Provider>
        </React.Fragment>
    );
};

Provider.PropTypes = {
    store: PropTypes.array.isRequired,
};

export default App;
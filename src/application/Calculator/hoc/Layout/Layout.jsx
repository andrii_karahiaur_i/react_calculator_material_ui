import React from 'react';

const Layout = props => {
    return(
        <div className="fhd-calculator my-5 pb-5">
            {props.children}
        </div>
    )
};

export default Layout;
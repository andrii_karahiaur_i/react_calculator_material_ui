import React from "react";
import clsx from 'clsx';

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from "@material-ui/core/FormHelperText";

const variables = {
    'red': '#EB5757',
    'green': '#0cad07',
    'gray_200': '#e0e0e0',
};

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            '& label.Mui-focused': {
                color: 'green',
            },
            '& .MuiInput-underline:after': {
                borderBottomColor: variables.green,
            },
            '& .MuiOutlinedInput-root': {
                '& fieldset': {
                    borderColor: variables.gray_200,
                },
                '&:hover fieldset': {
                    borderColor: variables.gray_200,
                },
                '&.Mui-focused fieldset': {
                    borderColor: variables.green,
                },
            }
        },
        selectField: {
            width: "100%"
        },
        formControl: {
            marginLeft: theme.spacing(0),
            marginRight: theme.spacing(0),
            marginBottom: theme.spacing(1)
        },
        formControl_danger: {
            '& .MuiOutlinedInput-root': {
                '& fieldset': {
                    borderColor: variables.red,
                }
            },
            '& .MuiFormHelperText-root': {
                color: variables.red,
            }
        },
        inputLabel: {
            backgroundColor: "#fff",
            paddingLeft: theme.spacing(0.5),
            paddingRight: theme.spacing(0.5)
        },
        danger: {
            '& .MuiFormHelperText-contained': {
                color: variables.red,
                marginLeft: '0'
            }
        }
    })
);

const renderFromHelper = ({ touched, error }) => {

    if (!(touched && error)) {
        return false;
    } else {
        return <FormHelperText>{touched && error}</FormHelperText>
    }
};

const SelectField = ({input, label, options, meta: { touched, error }, children, ...custom}) => {
    const classes = useStyles();
    const hasError = touched && error;

    return(
        <FormControl
            className={hasError ? clsx(classes.root, classes.formControl, classes.formControl_danger) : clsx(classes.root, classes.formControl)}
        >
            {/*<InputLabel>{label ? label : ""}</InputLabel>*/}
            <TextField
                className={classes.selectField}
                variant="outlined"
                select
                label={label ? label : ""}
                {...input}
                {...custom}
            >
                {options.map(option => (
                    <MenuItem key={option.id} value={option.id}>
                        {option.name}
                    </MenuItem>
                ))}
            </TextField>
            <div className={classes.danger}>{renderFromHelper({touched, error})}</div>
        </FormControl>
    )
};

export default SelectField;
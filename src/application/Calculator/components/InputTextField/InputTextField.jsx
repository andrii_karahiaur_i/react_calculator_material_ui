import React from "react";
import clsx from 'clsx';
// import TextField from '@material-ui/core/TextField';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import OutlinedInput from '@material-ui/core/OutlinedInput';


import FormHelperText from '@material-ui/core/FormHelperText';

const variables = {
    'red': '#EB5757',
    'green': '#0cad07',
    'gray_200': '#e0e0e0',
};

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            '& label.Mui-focused': {
                color: 'green',
            },
            '& .MuiInput-underline:after': {
                borderBottomColor: variables.green,
            },
            '& .MuiOutlinedInput-root': {
                '& fieldset': {
                    borderColor: variables.gray_200,
                },
                '&:hover fieldset': {
                    borderColor: variables.gray_200,
                },
                '&.Mui-focused fieldset': {
                    borderColor: variables.green,
                },
            }
        },
        textField: {
            width: "100%",
        },
        formControl: {
            width: '100%',
            marginLeft: theme.spacing(0),
            marginRight: theme.spacing(0),
            marginBottom: theme.spacing(1)
        },
        formControl_danger: {
            '& .MuiOutlinedInput-root': {
                '& fieldset': {
                    borderColor: variables.red,
                }
            },
            '& .MuiFormLabel-root': {
                color: variables.red,
            }
        },
        inputLabel: {
            backgroundColor: "#fff",
            paddingLeft: theme.spacing(0.5),
            paddingRight: theme.spacing(0.5)
        },
        danger: {
            '& .MuiFormHelperText-contained': {
                color: variables.red,
                marginLeft: '0'
            }
        }
    })
);

const renderFromHelper = ({ touched, error }) => {
    if (!(touched && error)) {
        return false;
    } else {
        return <FormHelperText>{touched && error}</FormHelperText>
    }
};

const InputTextField = ({label, input, type, placeholder, meta: { touched, invalid, error }, ...custom}) => {
    const classes = useStyles();
    const hasError = touched && error;
    return(
        <FormControl className={hasError ? clsx(classes.root, classes.formControl, classes.formControl_danger) : clsx(classes.root, classes.formControl)} variant="outlined">
            <InputLabel className={classes.inputLabel}>
                {label ? label : ""}
            </InputLabel>
            <OutlinedInput
                className={classes.textField}
                type={type ? type : "text"}
                placeholder={placeholder ? placeholder : ""}
                {...input}
                {...custom}
            />
            <div className={classes.danger}>{renderFromHelper({touched, error})}</div>
        </FormControl>
    )
};

export default InputTextField;
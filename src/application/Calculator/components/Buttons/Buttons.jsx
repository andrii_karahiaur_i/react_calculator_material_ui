import React, {Component} from 'react';
import Button from '@material-ui/core/Button';

class Buttons extends Component{

    render(){
        return (
            <Button
                variant="contained"
                color="primary"
                type="submit"
            >
                {this.props.children}
            </Button>
        )
    }
}

export default Buttons;
import React from "react";
import clsx from 'clsx';

import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const variables = {
    'red': '#EB5757',
    'green': '#0cad07',
    'gray_200': '#e0e0e0',
};

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {},
        checkboxField: {
            marginLeft: theme.spacing(0),
            marginRight: theme.spacing(0),
            marginBottom: theme.spacing(1),
        }
    })
);

const CheckboxField = ({ input, label }) => {
    const classes = useStyles();
    return(
        <FormControlLabel
            className={classes.checkboxField}
            control={
                <Checkbox
                    checked={input.value ? true : false}
                    onChange={input.onChange}
                />
            }
            label={label ? label : ""}
        />
    );
};

export default CheckboxField;
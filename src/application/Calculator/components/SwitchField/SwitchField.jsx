import React from 'react';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {},
		color: {}
	})
);

const SwitchField = (props) => {
	const [state, setState] = React.useState({
		checkedA: true,
		checkedB: true,
	});

	const handleChange = name => event => {
		setState({ ...state, [name]: event.target.checked });
	};

	const classes = useStyles();

	return (
		<FormGroup row>
			<FormControlLabel
				control={
					<Switch
						checked={state.checkedB}
						onChange={handleChange('checkedB')}
						value="checkedB"
						color="primary"
					/>
				}
				label="Primary"
			/>
		</FormGroup>
	);
};

export default SwitchField;
import React from "react";
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControl from '@material-ui/core/FormControl'
import FormControlLabel from '@material-ui/core/FormControlLabel'

const RadioFieldWays = ({input, ...rest}) => (
    <FormControl>
        <RadioGroup {...input} {...rest}>
            <FormControlLabel value="air" control={<Radio />} label="_Air" />
            <FormControlLabel value="water" control={<Radio />} label="_Water" />
        </RadioGroup>
    </FormControl>
)

export default RadioFieldWays;